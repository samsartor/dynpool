#!/bin/bash

BUMP=$(cargo semver --compact)
CURRENT=$(cargo metadata --no-deps --format-version=1 | grep -Po '"version":"[^"]+"')
if [ "\"version\":\"$BUMP\"" != $CURRENT ]; then
    echo "Vesion should be bumped to $BUMP"
    exit 1
fi