extern crate dynpool;
use dynpool::*;
use std::time::{Duration, Instant};

fn main() {
    let workers = func_worker(|index| {
        println!("New worker #{}", index);
        move || {
            println!("Hello from #{}", index);
            Decision::Again
        }
    });
    let sys = with_threads(workers, 10);
    let end_time = Instant::now() + Duration::from_millis(500);
    Pool::start_fg(shutdown_after(sys, end_time)).unwrap();
}
